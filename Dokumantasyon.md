
# Projenin  locale alınması

- git clone https://se1cuk@bitbucket.org/ilkayksc/yoklamasistemi.git

# Sanal Alan Kurulması

- pip install virtualenv

- python3 -m venv yenv

# Sanal Alanın Aktif Edilmesi

- source yenv/bin/activate

# Proje için gerekli paketlerin kurulması

- cd yoklama-backend

- pip install -r requirenments.txt

# Projenin Çalıştırılması

- cd squareCode

- ./manage.py runserver