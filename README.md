----
Sınıf ortamlarında yoklamaların kağıt üzerinde alınıyor . Bu proje ile birlikte hedefimiz kağıt üzerindeki yoklama sistemini kaldırarak teknolojiden faydalanrak telefon ile birlikte yoklama almayı planlıyoruz .

Kağıt ile yoklama alınırken imza atma esnasında Öğretim görevlisnin anlatmış olduğu ayrıntıyı kaçırabiliyoruz veya yazma esnasında geç kalabiliyoruz. Aynı zamanda gelmeyen arkadaşanın yerine imza atılabiliyor.

Hedefimizde dersin belirlenen sürede yoklamanın alınarak sınıfta olanların yoklamasını alarak yoklamayı daha gerçekçi kılmaktır .

----

# Projede Görev Alacak kişiler:

- 330210 - İlkay KESİCİ

- 314040 - Durmuş YAŞAR

- 314042 - Osman Selçuk KARSLI

- 313986 - Serkan ASLAN

----