import redis

from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, get_backends
from django.contrib.sites.shortcuts import get_current_site
from django.template import RequestContext
from django.shortcuts import render_to_response, render
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.urls import reverse
from django.contrib.auth.models import User

from django.conf import settings

from .utils import generate_random_string, salted_hash
from .qr import make_qr_code

AUTH_QR_CODE_EXPIRATION_TIME = getattr(
    settings,
    "AUTH_QR_CODE_EXPIRATION_TIME",
    300
)

AUTH_QR_CODE_REDIRECT_URL = getattr(
    settings,
    "AUTH_QR_CODE_REDIRECT_URL",
    "/"
)

AUTH_QR_CODE_REDIS_KWARGS = getattr(
    settings,
    "AUTH_QR_CODE_REDIS_KWARGS",
    {}
)


def qr_code_page(request, r=None):
    auth_code = generate_random_string(50)
    auth_code = auth_code.strip()
    auth_code_hash = salted_hash(auth_code)
    auth_code_hash = auth_code_hash.strip()

    key = "".join(["qrauth_", auth_code_hash])

    return render(request, "page.html",
                              {"auth_code": auth_code})


def qr_code_picture(request, auth_code, r=None):
    auth_code = auth_code.strip()
    auth_code_hash = salted_hash(auth_code)
    auth_code_hash = auth_code_hash.strip()

    key = "".join(["qrauth_", auth_code_hash])
    user_id = r.get(key)

    if (user_id == None) or (int(user_id.decode(), 10) != request.user.id):
        raise Http404("No such auth code")

    current_site = request.get_host()
    print("Current site 25", request.get_host())
    scheme = request.is_secure() and "https" or "http"

    login_link = "".join([
        scheme,
        "://",
        current_site,
        reverse("qr_code_login", args=(auth_code_hash,)),
    ])

    img = make_qr_code(login_link)
    response = HttpResponse(content_type="image/png")
    img.save(response, "PNG")
    return response