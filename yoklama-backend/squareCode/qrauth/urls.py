from django.urls import path, re_path
from .views import qr_code_picture, qr_code_page
from django.views.generic.base import TemplateView


urlpatterns = [
    re_path(
        r'^pic/(?P<auth_code>[a-zA-Z\d]{50})/$',
        qr_code_picture,
        name='auth_qr_code'
    ),

    re_path(
        r'invalid_code/$',
        TemplateView.as_view(
            template_name='invalid_code.html'
        ),
        name='invalid_auth_code'
    ),
    re_path(
        r'^$',
        qr_code_page,
        name='qr_code_page'
    ),
]