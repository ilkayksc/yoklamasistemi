# Django
from django.urls import path, include

# Third-Party
from rest_framework import routers

# Local Django
from users.api_views import UserViewSetV1
from rollcall.api_views import RollCallViewSetV1, StudentViewSetV1, LessonViewSetV1

router_V1 = routers.DefaultRouter()

LIST_V1 = [
    ('users', UserViewSetV1, 'users'),
    ('rollcalls', RollCallViewSetV1, 'rollcalls'),
    ('students', StudentViewSetV1, 'students'),
    ('lessons', LessonViewSetV1, 'lessons'),
]

for router in LIST_V1:
    router_V1.register(router[0], router[1], base_name=router[2])


urlpatterns = [
    path('v1/', include(router_V1.urls), name='v1'),
]

