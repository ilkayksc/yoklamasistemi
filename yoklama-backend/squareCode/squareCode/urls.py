"""
Bu sayfa Projeye ait linklerin olduğu sayfadır
admin/ : Bu link Django nun sağlamış olduğu yönetim panelinin linkidir.
rollcall/ : Bu da bizim uygulamamıza ait linkleri belirten include'dur.
user/ : Bu da bizim uygulamamıza ait linkleri belirten include'dur.
"""

# Django
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.views.static import serve


urlpatterns = [
    # Admin
    path('admin/', admin.site.urls),

    # Rollcall
    path('rollcall/', include('rollcall.urls'), name='rollcall'),
   
    # Api
    path('', include('squareCode.api_urls')),

    # Token
    path('auth/', include('djoser.urls.authtoken')),

    path('qr', include('qrauth.urls')),
]

# Media
if settings.DEBUG:
    urlpatterns += (
        path('media/(<path>.*)', serve, {
            'document_root': settings.MEDIA_ROOT,
        }),
    )