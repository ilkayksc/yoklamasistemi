# Local Django
from .base_api_views import StudentViewSet, RollCallViewSet, LessonViewSet
from rollcall.serializers import (
    StudentSerializer, RollCallSerializer, LessonSerializer,
    StudentSerializerV1, StudentListSerializerV1, StudentCreateSerializerV1,
    StudentRetrieveSerializerV1, StudentUpdateSerializerV1,
    RollCallSerializerV1, RollCallListSerializerV1, RollCallCreateSerializerV1,
    RollCallRetrieveSerializerV1, RollCallUpdateSerializerV1,
    LessonSerializerV1, LessonListSerializerV1, LessonCreateSerializerV1,
    LessonRetrieveSerializerV1, LessonUpdateSerializerV1
)


class StudentViewSetV1(StudentViewSet):

    def get_serializer_class(self):
        if self.action == 'list':
            return StudentListSerializerV1
        elif self.action == 'create':
            return StudentCreateSerializerV1
        elif self.action == 'retrieve':
            return StudentRetrieveSerializerV1
        elif self.action == 'update':
            return StudentUpdateSerializerV1
        else:
            return StudentSerializer


class RollCallViewSetV1(RollCallViewSet):

    def get_serializer_class(self):
        if self.action == 'list':
            return RollCallListSerializerV1
        elif self.action == 'create':
            return RollCallCreateSerializerV1
        elif self.action == 'retrieve':
            return RollCallRetrieveSerializerV1
        elif self.action == 'update':
            return RollCallUpdateSerializerV1
        else:
            return RollCallSerializer

class LessonViewSetV1(LessonViewSet):

    def get_serializer_class(self):
        if self.action == 'list':
            return LessonListSerializerV1
        elif self.action == 'create':
            return LessonCreateSerializerV1
        elif self.action == 'retrieve':
            return LessonRetrieveSerializerV1
        elif self.action == 'update':
            return LessonUpdateSerializerV1
        else:
            return LessonSerializer