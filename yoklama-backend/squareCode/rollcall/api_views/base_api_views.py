# Third-Party
from rest_framework import viewsets, mixins

# Local Django
from rollcall.models import *
from rollcall.serializers import (
    StudentSerializer, StudentListSerializer, StudentCreateSerializer,
    StudentRetrieveSerializer, StudentUpdateSerializer,
    RollCallSerializer, RollCallListSerializer, RollCallCreateSerializer,
    RollCallRetrieveSerializer, RollCallUpdateSerializer,
    LessonSerializer, LessonListSerializer, LessonCreateSerializer,
    LessonRetrieveSerializer, LessonUpdateSerializer
)


class StudentViewSet(mixins.ListModelMixin,
                      mixins.CreateModelMixin,
                      mixins.RetrieveModelMixin,
                      mixins.UpdateModelMixin,
                      mixins.DestroyModelMixin,
                      viewsets.GenericViewSet):
    queryset = Student.objects.all()

    def get_queryset(self):
        return self.queryset.filter(task__user=self.request.user)

    def get_serializer_class(self):
        if self.action == 'list':
            return StudentListSerializer
        elif self.action == 'create':
            return StudentCreateSerializer
        elif self.action == 'retrieve':
            return StudentRetrieveSerializer
        elif self.action == 'update':
            return StudentUpdateSerializer
        else:
            return StudentSerializer

    def perform_create(self, serializer):
        student = serializer.save()
        student.save()


    def perform_update(self, serializer):
        reminder = serializer.save()
        reminder.save()

class RollCallViewSet(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                  mixins.RetrieveModelMixin,
                  mixins.UpdateModelMixin,
                  mixins.DestroyModelMixin,
                  viewsets.GenericViewSet):
    queryset = RollCall.objects.all()

    def get_queryset(self):
        return self.queryset.filter(user=self.request.user)

    def get_serializer_class(self):
        if self.action == 'list':
            return RollCallListSerializer
        elif self.action == 'create':
            return RollCallCreateSerializer
        elif self.action == 'retrieve':
            return RollCallRetrieveSerializer
        elif self.action == 'update':
            return RollCallUpdateSerializer
        else:
            return RollCallSerializer

    def perform_create(self, serializer):
        rollcall = serializer.save(user=self.request.user)

        students_data = self.request.data.get('students', [])
        for student_data in students_data:
            Student.objects.create(rollcall=rollcall, **student_data)

    def perform_destroy(self, instance):
        students = instance.reminders.all()

        super(RollCallViewSet, self).perform_destroy(instance)


class LessonViewSet(mixins.ListModelMixin,
                  mixins.CreateModelMixin,
                  mixins.RetrieveModelMixin,
                  mixins.UpdateModelMixin,
                  mixins.DestroyModelMixin,
                  viewsets.GenericViewSet):
    queryset = Lesson.objects.all()

    def get_queryset(self):
        return self.queryset.filter(user=self.request.user)

    def get_serializer_class(self):
        if self.action == 'list':
            return LessonListSerializer
        elif self.action == 'create':
            return LessonCreateSerializer
        elif self.action == 'retrieve':
            return LessonRetrieveSerializer
        elif self.action == 'update':
            return LessonUpdateSerializer
        else:
            return LessonSerializer

    def perform_create(self, serializer):
        lesson = serializer.save(user=self.request.user)

        rollcalls_data = self.request.data.get('rollcalls', [])
        for rollcall_data in rollcalls_data:
            RollCall.objects.create(lesson=lesson, **rollcall_data)

    def perform_destroy(self, instance):
        rollcalls = instance.reminders.all()

        super(LessonViewSet, self).perform_destroy(instance)