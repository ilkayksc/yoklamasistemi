# Local Django
from .base_api_views import StudentViewSet, RollCallViewSet,LessonViewSet
from .v1_api_views import StudentViewSetV1, RollCallViewSetV1, LessonViewSetV1