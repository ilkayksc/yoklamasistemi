from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import ValidationError

from datetime import datetime    

from users.models import User


EDUCATION = (
    ('ONE', _('1.Öğretim')),
    ('TWO', _('2.Öğretim')),
)
# Öğrenci Bilgileri
class Student(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    s_number = models.IntegerField(verbose_name=_('Öğreenci Numarası'))
    education = models.CharField(verbose_name=_(' Öğretim'), max_length=15, choices=EDUCATION)

    def __str__(self):
        return '{s_number} - {education}'.format(
            s_number=self.s_number,
            education=self.education
        )

    def clean(self):
        temp = Student.objects.filter(s_number=self.s_number)
        for i in temp:
            if i.s_number == self.s_number:
                raise ValidationError({'s_number': 'Sisteme kayıtlı.'})

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = 'Student'
        verbose_name_plural = 'Student'


# Yoklama Bilgileri
class RollCall(models.Model):
    student = models.ManyToManyField(Student)
    date = models.DateTimeField(verbose_name=_('Tarih'), default=datetime.now,)
    r_number = models.PositiveIntegerField(verbose_name=_('Yoklama Katılım Sayısı'), default=0)

    def __str__(self):
        student = '-'.join([str(student.s_number) for student in self.student.all()])
        return '{student} - {r_number}'.format(
            student=student,
            r_number=self.r_number
        )

    class Meta:
        verbose_name = 'RollCall'
        verbose_name_plural = 'RollCall'

# Ders Bilgileri
class Lesson(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    rollcall = models.ForeignKey(RollCall, on_delete=models.CASCADE)
    l_student = models.ManyToManyField(Student)
    l_code = models.CharField(verbose_name=_('Ders Kodu'), max_length=10)
    l_name = models.CharField(verbose_name=_('Ders Ad'), max_length=50)
    max_discontinuity = models.IntegerField(verbose_name=_('Maksimum Devamsızlık Hakkı'))
    is_activate = models.BooleanField(verbose_name=_('Ders Aktif mi ?'), default=False)

    def __str__(self):
        return '{l_code} - {l_name}'.format(
            l_code=self.l_code,
            l_name=self.l_name
        )

    def clean(self):
        temp = Lesson.objects.filter(l_code=self.l_code)
        for i in temp:
            if i.l_code == self.l_code:
                raise ValidationError({'l_code': 'Sisteme kayıtlı.'})

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)

    class Meta:
        verbose_name = 'Lesson'
        verbose_name_plural = 'Lesson'