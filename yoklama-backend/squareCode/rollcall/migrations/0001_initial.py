# Generated by Django 2.1.4 on 2018-12-19 16:43

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Lesson',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('l_code', models.CharField(max_length=10, verbose_name='Ders Kodu')),
                ('l_name', models.CharField(max_length=50, verbose_name='Ders Ad')),
                ('max_discontinuity', models.IntegerField(verbose_name='Maksimum Devamsızlık Hakkı')),
                ('is_activate', models.BooleanField(default=False, verbose_name='Ders Aktif mi ?')),
            ],
        ),
        migrations.CreateModel(
            name='RollCall',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateTimeField(auto_now=True, verbose_name='Tarih')),
                ('r_number', models.PositiveIntegerField(editable=False, verbose_name='Yoklama Katılım Sayısı')),
            ],
        ),
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('s_number', models.IntegerField(verbose_name='Öğreenci Numarası')),
                ('education', models.CharField(choices=[('ONE', '1.Öğretim'), ('TWO', '2.Öğretim')], max_length=15, verbose_name='Ders Kodu')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='rollcall',
            name='student',
            field=models.ManyToManyField(to='rollcall.Student'),
        ),
        migrations.AddField(
            model_name='lesson',
            name='l_student',
            field=models.ManyToManyField(to='rollcall.Student'),
        ),
        migrations.AddField(
            model_name='lesson',
            name='rollcall',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='rollcall.RollCall'),
        ),
        migrations.AddField(
            model_name='lesson',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
