from django.contrib import admin
from .models import *
from django.utils.translation import ugettext_lazy as _

# Öğrenci
@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    fields = ('user', 's_number', 'education')
    list_display = ('user', 's_number', 'education')
    list_filter = ('user', 'education')
    search_fields = ('user', 's_number ')

# Yoklama
@admin.register(RollCall)
class RollCallAdmin(admin.ModelAdmin):
    fields = ('student', 'date', 'r_number')
    list_display = ('get_student', 'date', 'r_number')
    list_filter = ('student', 'date')
    search_fields = ('student',)

    def get_student(self, obj):
        student = '-'.join(
            [student.education for student in obj.student.all()]
        )
        return student
# Ders
@admin.register(Lesson)
class LessonAdmin(admin.ModelAdmin):
    fields = ('user', 'rollcall', 'l_student','l_code', 'l_name', 'max_discontinuity', 'is_activate')
    list_display = ('user','l_code', 'l_name')
    list_filter = ('l_code', 'l_name')
    list_filter = ('l_code', 'l_name')
    search_fields = ('user', 'rollcall', 'l_student','l_code', 'l_name',)

    def get_student(self, obj):
        student = '-'.join(
            [student.education for student in obj.student.all()]
        )
        return student