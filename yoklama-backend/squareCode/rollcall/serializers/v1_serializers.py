from .base_serializers import (
    StudentSerializer, StudentListSerializer, StudentCreateSerializer,
    StudentRetrieveSerializer, StudentUpdateSerializer,
    RollCallSerializer, RollCallListSerializer, RollCallCreateSerializer,
    RollCallRetrieveSerializer, RollCallUpdateSerializer,
    LessonSerializer, LessonListSerializer, LessonCreateSerializer,
    LessonRetrieveSerializer, LessonUpdateSerializer
)
from users.serializers import *

class StudentSerializerV1(StudentSerializer):
    pass

class StudentListSerializerV1(StudentListSerializer):
    pass


class StudentCreateSerializerV1(StudentCreateSerializer):
    pass


class StudentRetrieveSerializerV1(StudentRetrieveSerializer):
    pass


class StudentUpdateSerializerV1(StudentUpdateSerializer):
    pass

class RollCallSerializerV1(RollCallSerializer):
    pass


class RollCallListSerializerV1(RollCallListSerializer):
    pass


class RollCallCreateSerializerV1(RollCallCreateSerializer):
    students = StudentCreateSerializerV1(many=True, read_only=True)


class RollCallRetrieveSerializerV1(RollCallRetrieveSerializer):
    students = StudentRetrieveSerializerV1(many=True, read_only=True)


class RollCallUpdateSerializerV1(RollCallUpdateSerializer):
    pass

class LessonSerializerV1(LessonSerializer):
    pass
    
class LessonListSerializerV1(LessonListSerializer):
    pass


class LessonCreateSerializerV1(LessonCreateSerializer):
    users = UserCreateSerializerV1(many=True, read_only=True)
    rolcalls = RollCallCreateSerializerV1(many=True, read_only=True)
    l_studentss = StudentCreateSerializerV1(many=True, read_only=True)


class LessonRetrieveSerializerV1(LessonRetrieveSerializer):
    users = UserRetrieveSerializerV1(many=True, read_only=True)
    rolcalls = RollCallRetrieveSerializerV1(many=True, read_only=True)
    l_studentss = StudentRetrieveSerializerV1(many=True, read_only=True)


class LessonUpdateSerializerV1(LessonUpdateSerializer):
    pass