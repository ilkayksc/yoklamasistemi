# Standart Library
import datetime

# Third-Party
from rest_framework import serializers

# Django
from django.utils.translation import ugettext_lazy as _

# Local Django
from rollcall.models import Student, RollCall, Lesson


class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Student
        fields = ('id', 'user', 's_number', 'education')

    def validate_user(self, value):
        user = self.context['request'].user

        if user != value.user:
            raise serializers.ValidationError(_('Not found.'))

        return value

class StudentListSerializer(StudentSerializer):
    pass


class StudentCreateSerializer(StudentSerializer):

    class Meta:
        model = Student
        fields = ('id', 'user', 's_number', 'education')


class StudentRetrieveSerializer(StudentSerializer):
    pass


class StudentUpdateSerializer(StudentSerializer):

    class Meta:
        model = Student
        fields = ('id', 'user', 's_number', 'education')


class RollCallSerializer(serializers.ModelSerializer):
    date = serializers.DateTimeField(error_messages={
        'invalid': _('Datetime has wrong format.')
    })

    class Meta:
        model = RollCall
        fields = ('id', 'date', 'r_number')

    def validate_student(self, value):
        user = self.context['request'].user

        if user != value.user:
            raise serializers.ValidationError(_('Not found.'))

        return value

    def validate_date(self, value):
        if value <= datetime.datetime.utcnow():
            raise serializers.ValidationError(_('Can not add past rollcall.'))

        return value


class RollCallListSerializer(RollCallSerializer):
    pass


class RollCallCreateSerializer(RollCallSerializer):

    class Meta:
        model = RollCall
        fields = ('id', 'date', 'r_number')


class RollCallRetrieveSerializer(RollCallSerializer):
    pass


class RollCallUpdateSerializer(RollCallSerializer):

    class Meta:
        model = RollCall
        fields = ('id', 'date', 'r_number')

    def validate_date(self, value):
        super(RollCallUpdateSerializer, self).validate_date(value)

        if value == self.instance.date:
            raise serializers.ValidationError(_('Can not select same rollcall.'))

        return value


class LessonSerializer(serializers.ModelSerializer):
    class Meta:
        model = Lesson
        fields = ('id', 'user', 'rollcall', 'l_student', 'l_code', 'l_name', 'max_discontinuity','is_activate')

    def validate_rollcall(self, value):
        user = self.context['request'].user

        if user != value.user:
            raise serializers.ValidationError(_('Not found.'))

        return value

    def validate_l_student(self, value):
        user = self.context['request'].user

        if user != value.user:
            raise serializers.ValidationError(_('Not found.'))

        return value


class LessonListSerializer(LessonSerializer):
    pass


class LessonCreateSerializer(LessonSerializer):

    class Meta:
        model = Lesson
        fields = ('id', 'user', 'rollcall', 'l_student', 'l_code', 'l_name', 'max_discontinuity','is_activate')


class LessonRetrieveSerializer(LessonSerializer):
    pass


class LessonUpdateSerializer(LessonSerializer):

    class Meta:
        model = Lesson
        fields = ('id', 'user', 'rollcall', 'l_student', 'l_code', 'l_name', 'max_discontinuity','is_activate')
