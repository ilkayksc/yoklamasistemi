# Local Django
from .base_serializers import (
    StudentSerializer, StudentListSerializer, StudentCreateSerializer,
    StudentRetrieveSerializer, StudentUpdateSerializer,
    RollCallSerializer, RollCallListSerializer, RollCallCreateSerializer,
    RollCallRetrieveSerializer, RollCallUpdateSerializer,
    LessonSerializer, LessonListSerializer, LessonCreateSerializer,
    LessonRetrieveSerializer, LessonUpdateSerializer
)
from .v1_serializers import (
    StudentSerializerV1, StudentListSerializerV1, StudentCreateSerializerV1,
    StudentRetrieveSerializerV1, StudentUpdateSerializerV1,
    RollCallSerializerV1, RollCallListSerializerV1, RollCallCreateSerializerV1,
    RollCallRetrieveSerializerV1, RollCallUpdateSerializerV1,
    LessonSerializerV1, LessonListSerializerV1, LessonCreateSerializerV1,
    LessonRetrieveSerializerV1, LessonUpdateSerializerV1
)